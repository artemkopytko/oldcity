/**
 * Created by artemkopytko on 11/11/17.
 */
/**
 * Created by artemkopytko on 12.03.17.
 */
$(document).ready(function () {
    $(function() {
        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1500);
                    console.log();
                    return false;
                }
            }
        });
    });
});