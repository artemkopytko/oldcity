$(document).ready(function(){
    var toggled = false;

    $('#nav-icon').click(function(){
        $(this).toggleClass('open');
        $('.navigation-list').toggleClass('open');

        if(!toggled) {
            $( ".navigation-list li a" ).each(function(index) {
                $(this).on("click", function(){
                    $('#nav-icon').toggleClass('open');
                    $('.navigation-list').toggleClass('open');
                });
            });
        }
        toggled = !toggled;
    });

});