<!DOCTYPE html>
<html lang="ru">
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="text/html;charset=UTF-8;ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, user-scalable=no">
<meta name="keywords" content="Ресторан, Старый Город, Одесса, Кафе, Свадьба, Банкет, Юбилеи, Караоке">
<meta name="description" content='Ресторан "Старый Город" расположился на уютной улице в районе старой Одессы. Ресторан выполнен в стиле старого города, что заметно в интерьере с первого взгляда.'>
<meta name="robots" content="index,follow">

<link rel="icon" type="image/png" href="<?=get_field('favicon');?>" />

<title>Ресторан "Старый Город" Одесса | Свадьбы и банкеты</title>


<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=cyrillic" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126729768-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-126729768-1');
</script>


<?php wp_head(); ?>
</head>
<body>

<div class="wrapper">
    <div class="content">
        <nav class="navigation">
            <div id="nav-icon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="container navigation-content">
            <ul class="navigation-list">
                <li><a href="/">Главная</a></li>
                <li><a href="/#about">О Нас</a></li>
                <li class="nav-image"><a class="opacity_1" href="/"><img src="<?=get_field('navigation_image');?>" alt="Логотип Ресторана"></a></li>
                <li><a href="/#menu">Меню</a></li>
                <li><a href="#contacts">Контакты</a></li>
            </ul>
            </div>
        </nav>
